<%@page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<html>
    <head>
         <meta charset="UTF-8">
         <title>Delete User</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
             rel="stylesheet"
             integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
             crossorigin="anonymous">
     </head>
     <body>
                  <p class="text-center">Вы действительно хотите удалить пользователя ${user.get("firstName")}
                   ${user.get("lastName")} ?</p>
                   <form action="delete?id=${user.get("id")}" method="post">
                       <button type="submit" class="btn btn-danger">Удалить</button>
                   </form>

     </body>
 </html>
<!-- END -->
