<%@page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN -->
<!DOCTYPE html>
<html>
    <head>
         <meta charset="UTF-8">
         <title>User</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
             rel="stylesheet"
             integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
             crossorigin="anonymous">
     </head>
     <body>
     <div class="container -sm">
                  <table class="table table-hover">
                            <thead>
                              <tr>
                                <th scope="col">fullName</th>
                              </tr>
                            </thead>
                           <c:forEach var="users" items="${users}">
                            <tbody class="table-striped">
                              <tr>
                                <td><a href="users/show?id=${users.get("id")}">
                                ${users.get("firstName")} ${users.get("lastName")}</a></td>
                              </tr>
                            </tbody>
                           </c:forEach>
                          </table>
                 </div>
     </body>
 </html>
<!-- END -->
