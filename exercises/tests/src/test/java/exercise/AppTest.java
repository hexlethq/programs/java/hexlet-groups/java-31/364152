package exercise;


import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AppTest {
    private List<Integer> expected;

    @BeforeEach
    void listForTestTake() {
        this.expected = new ArrayList<>();
    }

    @Test
    void testTake1() {
        // BEGIN
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        expected.add(1);
        expected.add(2);
        List<Integer> actual = App.take(numbers1, 2);
        Assertions.assertEquals(actual, expected);
        expected.clear();
        // END
    }

    @Test
    void testTake2() {
        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        List<Integer> actual = App.take(numbers2, 8);
        expected = numbers2;
        Assertions.assertEquals(actual, expected);
        expected.clear();
    }

    @Test
    void testTake3() {
        List<Integer> numbers3 = new ArrayList<>(Arrays.asList());
        List<Integer> actual = App.take(numbers3, 4);
        expected = numbers3;
        Assertions.assertEquals(actual, expected);
        expected.clear();
    }
}
