package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        var firstСondition = (a == b) && (a != c) && (b != c);
        var secondCondition = (b == c) && (c != a) && (b != a);
        var thirdCondition = (c == a) && (c != b) && (a != b);
        var result = "";
        if (((a + b) <= c) || ((c + a) <= b) || ((b + c) <= a)) {
            return "Треугольник не существует";
        } else if (firstСondition || secondCondition || thirdCondition) {
            return "Равнобедренный";
        } else if (((a == b) && (a == c) && (c == b))) {
            return "Равносторонний";
        } else if ((a != b) && (a != c) && (c != b)) {
            return "Разносторонний";
        }
        return result;
    }

    public static String getFinalGrade(int exam, int project) {
        if ((exam > 90) || (project > 10)) {
            return "100";
        } else if ((exam > 75) && (project >= 5)) {
            return "90";
        } else if ((exam > 50) && (project >= 2)) {
            return "75";
        }
        return "0";

    }
}
