package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        int[] arr1 = new int[arr.length];
        for (int i = arr.length - 1; 0 <= i && arr.length != 0; i--) {
            arr1[arr.length - 1 - i] = arr[i];
        }
        return arr1;
        
    }

    public static int mult(int[] numbers) {
        int result = 1;
        for (int i = 0; i < numbers.length; i++) {
            result = result * numbers[i];
        }
        return result;
    }
    // END
}
