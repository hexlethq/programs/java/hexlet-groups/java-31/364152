package exercise;

import java.lang.reflect.Field;
import java.util.*;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> result = new ArrayList<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (field.getAnnotation(NotNull.class) != null && Objects.equals(field.get(address), null)) {
                    result.add(field.getName());
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> result = new HashMap<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            try {
                field.setAccessible((true));
                if (field.getAnnotation(NotNull.class) != null && Objects.equals(field.get(address), null)) {
                    result.put(field.getName(), new ArrayList<>(Collections.singleton("can not be null")));
                    break;
                }
                if (field.getAnnotation(MinLength.class) != null &&
                        field.get(address).toString().length() < field.getAnnotation(MinLength.class).minLength()) {
                    result.put(field.getName(), new ArrayList<>(Collections.singleton("length less than " +
                            field.getAnnotation(MinLength.class).minLength())));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
// END
