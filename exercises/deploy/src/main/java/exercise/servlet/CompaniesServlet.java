package exercise.servlet;

import exercise.Data;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

@WebServlet("/companies")
public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        PrintWriter out = response.getWriter();
        List<String> result = new ArrayList<String>(Data.getCompanies());
        if (request.getQueryString() == null || !request.getQueryString().startsWith("search")
                || request.getParameter("search") == null) {
            for (String res : result) {
                out.println((res + "\n").trim());
            }
        } else {
            List<String> rr = result.stream()
                    .filter(x -> x.contains(request.getParameter("search")))
                    .toList();
            if (rr.size() == 0) {
                out.println("Companies not found");
            } else {
                for (String result1 : rr) {
                    out.println((result1 + "\n").trim());
                }
            }
        }
        // END
    }
}
