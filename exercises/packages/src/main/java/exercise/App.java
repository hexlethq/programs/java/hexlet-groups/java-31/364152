// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;


public class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] midPoint = new double[2];
        midPoint[0] = (segment[0][0] + segment[1][0]) / 2;
        midPoint[1] = (segment[0][1] + segment[1][1]) / 2;

        return midPoint;
    }

    public static double[][] reverse(double[][] segment) {
        double[][] reverseSegment = new double[2][2];
        reverseSegment[0][0] = segment[1][0];
        reverseSegment[1][0] = segment[0][0];
        reverseSegment[0][1] = segment[1][1];
        reverseSegment[1][1] = segment[0][1];

        return reverseSegment;
    }
}
// END
