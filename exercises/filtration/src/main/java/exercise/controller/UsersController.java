package exercise.controller;

import com.querydsl.core.types.Predicate;
import exercise.model.User;
import exercise.model.QUser;
import exercise.repository.UserRepository;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

// Зависимости для самостоятельной работы
// import org.springframework.data.querydsl.binding.QuerydslPredicate;
// import com.querydsl.core.types.Predicate;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    // BEGIN
    @GetMapping(path = "")
    public List<User> filtrationUsers(@Parameter String firstName,
                                      @Parameter String lastName) {
        if (firstName == null && lastName == null) {
            return userRepository.findAll();
        } else if (firstName != null && lastName == null) {
            return (List<User>) userRepository.findAll(QUser.user.firstName.containsIgnoreCase(firstName));
        } else if (firstName == null) {
            return (List<User>) userRepository.findAll((QUser.user.lastName.containsIgnoreCase(lastName)));
        }
        return (List<User>) userRepository.findAll(QUser.user.firstName.containsIgnoreCase(firstName)
                .and(QUser.user.lastName.containsIgnoreCase(lastName))
        );
    }


    @GetMapping("/iw")
    public Iterable<User> filtrationUsersIw (@QuerydslPredicate(root = User.class) Predicate predicate) {
       return userRepository.findAll(predicate);
    }
    // END
}

