package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// BEGIN
class App {
    public static List findWhere(List<Map<String, String>> books, Map<String, String> where) {
        List<Map<String, String>> result = new ArrayList<>();
        for (Map<String, String> book : books) {
            boolean match = true;
            for (Map.Entry<String, String> field : where.entrySet()) {
                if (match) {
                    match = book.get(field.getKey()).equals(field.getValue());
                }
            }
            if (match) {
                result.add(book);
            }
        }
        return result;
    }
}
//END
