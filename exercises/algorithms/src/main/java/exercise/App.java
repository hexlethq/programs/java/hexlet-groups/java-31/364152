package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] number) {
        int temp;
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < number.length - 1; i++) {
                if (number[i] > number[i + 1]) {
                    temp = number[i];
                    number[i] = number[i + 1];
                    number[i + 1] = temp;
                    isSorted = false;
                }

            }
        }
        return number;
    }
    // END
}
