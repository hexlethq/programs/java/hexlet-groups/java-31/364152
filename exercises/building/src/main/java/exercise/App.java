package exercise;

import com.google.gson.Gson;

// BEGIN
public class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

    public static String toJson(String[] fruits) {
        Gson gson = new Gson();
        return gson.toJson(fruits);
    }

}
// END
