package exercise;

import java.util.Arrays;


// BEGIN
public class Kennel {
    private static String[][] puppies = new String[0][];
    private static int puppyCount = 0;

    public static void addPuppy(String[] puppy) {
        puppies = Arrays.copyOf(puppies, puppies.length + 1);
        puppies[puppies.length - 1] = puppy;
        puppyCount++;
    }

    public static void addSomePuppies(String[][] puppiesArr) {
        puppies = Arrays.copyOf(puppies, puppies.length + puppiesArr.length);

        for (String[] puppy : puppiesArr) {
            puppies[puppyCount] = puppy;
            puppyCount++;
        }
    }

    public static int getPuppyCount() {
        return puppyCount;
    }

    public static Boolean isContainPuppy(String namePuppy) {
        for (int i = 0; i < puppies.length; i++) {
            if (namePuppy.equals(puppies[i][0])) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        int countPuppy = 0;
        for (int i = 0; i < puppies.length; i++) {
            if (breed.equals(puppies[i][1])) {
                countPuppy++;
            }
        }
        String[] name = new String[countPuppy];
        int resultName = 0;
        for (int i = 0; i < puppies.length; i++) {
            if (breed.equals(puppies[i][1])) {
                name[resultName] = puppies[i][0];
                resultName++;
            }
        }
        return name;
    }

    public static void resetKennel() {
        puppies = new String[0][];
        puppyCount = 0;
    }
}
// END
