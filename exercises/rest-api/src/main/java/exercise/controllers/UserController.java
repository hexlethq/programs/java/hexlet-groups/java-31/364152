package exercise.controllers;

import io.javalin.core.validation.BodyValidator;
import io.javalin.core.validation.JavalinValidation;
import io.javalin.core.validation.ValidationError;
import io.javalin.core.validation.Validator;
import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import exercise.domain.User;
import exercise.domain.query.QUser;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> users = new QUser()
                .orderBy()
                .id
                .asc().findList();
        String json = DB.json().toJson(users);
        ctx.json(json);
        // END
    }

    ;

    public void getOne(Context ctx, String id) {

        // BEGIN
        User user = new QUser()
                .id.equalTo(Long.parseLong(id))
                .findOne();

        String json = DB.json().toJson(user);
        ctx.json(json);
        // END
    }

    ;

    public void create(Context ctx) {

        // BEGIN
        User userValidator = ctx.bodyValidator(User.class)
                .check(obj -> !obj.getFirstName().isEmpty(), "Поле не должно быть пустым")
                .check(obj -> !obj.getLastName().isEmpty(), "Поле не должно быть пустым")
                .check(obj -> EmailValidator.getInstance().isValid(obj.getEmail()), "email введен некоректно")
                .check(obj -> {
                    if (obj != null) {
                        return obj.getPassword().length() > 4;
                    }
                    return false;
                }, "пароль должен быть больше четырех символов")
                .check(obj -> {
                    if (obj != null) {
                        return StringUtils.isNumeric(obj.getPassword());
                    }
                    return false;
                }, "пароль должен состоять из цифр")
                .get();

        userValidator.save();

        // END
    }

    ;

    public void update(Context ctx, String id) {
        // BEGIN
        String jsonResponse = ctx.body();
        User userFind = DB.json().toBean(User.class, jsonResponse);
        String firstName = userFind.getFirstName();
        String lastName = userFind.getLastName();
        String email = userFind.getEmail();
        String password = userFind.getPassword();
        ;

        new QUser()
                .id.equalTo(Long.parseLong(id))
                .asUpdate()
                .set("first_name", firstName)
                .set("last_name", lastName)
                .set("email", email)
                .set("password", password)
                .update();
        // END
    }

    ;

    public void delete(Context ctx, String id) {
        // BEGIN
        new QUser()
                .id.equalTo(Long.parseLong(id))
                .delete();
        // END
    }

    ;
}
