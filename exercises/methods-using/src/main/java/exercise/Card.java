package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        var quantityStars = "*".repeat(starsCount);
        String cardNumberX = cardNumber.substring(12, 16);
        System.out.println(quantityStars + cardNumberX);

        // END
    }
}
