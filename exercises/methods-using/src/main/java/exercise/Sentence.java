package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        var lastChar = sentence.charAt(sentence.length() - 1);
        if (lastChar == '!') {
            System.out.println(sentence.toUpperCase());
        } else {
            System.out.println(sentence.toLowerCase());
        }

        // END
    }
}
