package exercise;

// BEGIN
public class NegativeRadiusException extends Exception{
    private final String error;

    public NegativeRadiusException(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
// END
