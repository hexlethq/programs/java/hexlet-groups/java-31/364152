package exercise;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Collections;

// BEGIN
public class App {

    public static void save(Path path, Car car) throws IOException {
        String resultJson = car.serialize();
        Files.write(path, Collections.singleton(resultJson));
    }

    public static Car extract(Path path) throws IOException {
        String ss = Files.readString(path);
        return Car.unserialize(ss);
    }

}
// END
