package exercise;

class Converter {
    // BEGIN
    public static Integer convert(Integer num, String str) {
        if (str.equals("b")) {
            return num * 1024;
        } else if (str.equals("Kb")) {
            return num / 1024;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
