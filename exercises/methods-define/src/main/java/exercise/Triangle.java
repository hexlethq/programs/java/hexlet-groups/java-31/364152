package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int angle) {
        double angle1 = (angle * Math.PI) / 180;
        double result = (a * b) / 2 * Math.sin(angle1);
        return result;
    }
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
