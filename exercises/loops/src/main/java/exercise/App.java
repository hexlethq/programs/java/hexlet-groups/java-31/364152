package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        var str1 = str.trim();
        var index = 0;
        String result = "";
        while (index < str1.length()) {
            if (Character.isSpaceChar(str1.charAt(index))) {
                var index1 = index + 1;
                char c = str1.charAt(index1);
                result = result + Character.toUpperCase(c);
            }
            index++;
        }
        return Character.toUpperCase(str1.charAt(0)) + result.replaceAll("\\s+", "");
    }
}
// END
