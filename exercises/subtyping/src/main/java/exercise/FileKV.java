package exercise;

import java.util.Map;

// BEGIN
public class FileKV  implements KeyValueStorage{


    private final String path;
    private final Map<String,String> memory;

    public FileKV( String path ,Map<String, String> memory) {
        this.path = path;
        this.memory = memory;
    }

    @Override
    public void set(String key, String value) {

    }

    @Override
    public void unset(String key) {

    }

    @Override
    public String get(String key, String defaultValue) {
        return null;
    }

    @Override
    public Map<String, String> toMap() {
        return null;
    }

    public Map<String, String> getMemory() {
        return memory;
    }

    public String getPath() {
        return path;
    }
}
// END
