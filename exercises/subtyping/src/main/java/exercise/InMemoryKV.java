package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class InMemoryKV implements KeyValueStorage {

    private Map<String, String> memory;

    public InMemoryKV(Map<String, String> memory) {
        this.memory = new HashMap<>(memory);
    }


    @Override
    public void set(String key, String value) {
        Map<String,String> tmp = new HashMap<>(Map.copyOf(memory));
        tmp.put(key,value);
        memory = Map.copyOf(tmp);
    }

    @Override
    public void unset(String key) {
        Map<String,String> tmp = new HashMap<>(Map.copyOf(memory));
        tmp.remove(key);
        memory = Map.copyOf(tmp);
    }

    @Override
    public String get(String key, String defaultValue) {
        if (memory.containsKey(key)) {
            return memory.get(key);
        }
        return defaultValue;
    }

    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(memory);
    }

    public Map<String, String> getMemory() {
        return memory;
    }
}
// END
