package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage storage) {
        Map<String, String> tmp = new HashMap<>();
        Map<String, String> mapStorage = storage.toMap();
        Set<String> keyFromStorage = storage.toMap().keySet();
        for (String key : keyFromStorage) {
            tmp.put(mapStorage.get(key), key);
            storage.unset(key);
        }
        Set<String> tmpSet = tmp.keySet();
        for (String key : tmpSet) {
            storage.set(key, tmp.get(key));
        }
    }
}
// END
