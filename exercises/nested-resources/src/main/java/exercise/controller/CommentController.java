package exercise.controller;

import exercise.ResourceNotFoundException;
import exercise.model.Comment;
import exercise.model.Post;
import exercise.repository.CommentRepository;
import exercise.repository.PostRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/posts")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    // BEGIN
    @GetMapping(path = "/{postId}/comments")
    public Iterable<Comment> getComments(@PathVariable long postId) {

        Iterable<Comment> ss = commentRepository.findAllByPostId(postId);

        return commentRepository.findAllByPostId(postId);
    }

    @GetMapping(path = "/{postId}/comments/{commentId}")
    public Comment getComment(@PathVariable long postId, @PathVariable long commentId) {

        Comment comment = commentRepository.findByIdAndPostId(commentId, postId);

        if (comment == null) {
            throw new ResourceNotFoundException("Comment not found");
        }

        return comment;
    }

    @PostMapping(path = "/{postId}/comments")
    public void createComment(@RequestBody Comment comment,
                              @PathVariable long postId) {
        Post post = postRepository.findById(postId).get();
        comment.setPost(post);
        commentRepository.save(comment);
    }

    @PatchMapping(path = "/{postId}/comments/{commentId}")

    public void updateComment(@RequestBody Comment newComment,
                              @PathVariable long postId,
                              @PathVariable long commentId) {
        Comment updateComment = commentRepository.findByIdAndPostId(commentId, postId);

        if (updateComment == null) {
            throw new ResourceNotFoundException("Comment not found");
        }
        updateComment.setContent(newComment.getContent());
        commentRepository.save(updateComment);

    }

    @DeleteMapping(path = "/{postId}/comments/{commentId}")

    public void deleteComment(@PathVariable long postId, @PathVariable long commentId) {

        Comment deleteComment = commentRepository.findByIdAndPostId(commentId, postId);
        if (deleteComment == null) {
            throw new ResourceNotFoundException("Comment not found");
        }
        commentRepository.delete(deleteComment);

    }

    // END
}
