package exercise;

import java.util.*;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> list, int num) {
        List<String> result = new ArrayList<>();
        if (list.isEmpty()) {
            return result;
        }
        Comparator<Home> comparator = Comparator.comparing(Home::getArea);
        list.sort(comparator);
        for (int i = 0; i < num; i++) {
            result.add(list.get(i).toString());
        }
        return result;
    }
}
// END
