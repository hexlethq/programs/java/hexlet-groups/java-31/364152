package exercise;

// BEGIN
public class Flat implements Home {

    private final double area;
    private final double balconyArea;
    private final int floor;

    public Flat(double area, double balconyArea, int floor) {
        this.area = area;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }

    public double getAreaFlat() {
        return area;
    }

    public double getBalconyArea() {
        return balconyArea;
    }

    public int getFloor() {
        return floor;
    }

    @Override
    public double getArea() {
        return getAreaFlat() + getBalconyArea();
    }

    @Override
    public int compareTo(Home home) {
        if (getArea() > home.getArea()) {
            return 1;
        } else if (getArea() < home.getArea()) {
            return -1;
        }
        return 0;
    }

    public String toString() {
        return "Квартира площадью " + getArea() + " метров на " + getFloor() + " этаже";
    }
}
// END
