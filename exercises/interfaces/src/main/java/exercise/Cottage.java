package exercise;

// BEGIN
public class Cottage implements Home {
    private final double area;
    private final int floorCount;

    public Cottage(double area, int floorCount) {
        this.area = area;
        this.floorCount = floorCount;
    }


    public double getAreaCottage() {
        return area;
    }

    public int getFloorCount() {
        return floorCount;
    }

    @Override
    public double getArea() {
        return getAreaCottage();
    }

    @Override
    public int compareTo(Home home) {
        if (getArea() > home.getArea()) {
            return 1;
        } else if (getArea() < home.getArea()) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return getFloorCount() + " этажный коттедж площадью " + getAreaCottage() + " метров";
    }
}
// END
