package exercise;

import java.lang.reflect.Proxy;

import exercise.calculator.CalculatorImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private Map<String, String> beanInfo = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            Inspect annotation = bean.getClass().getAnnotation(Inspect.class);
            beanInfo.put(beanName, annotation.level());
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (!beanInfo.containsKey(beanName)) {
            return bean;
        }
        final Logger logger = LoggerFactory.getLogger(beanName);
        String levelLog = beanInfo.get(beanName);
        return Proxy.newProxyInstance(
                CalculatorImpl.class.getClassLoader(),
                CalculatorImpl.class.getInterfaces(),
                (proxy, method, args) -> {
                    if (levelLog.equals("info")) {
                        logger.info("Was called method: " + method.getName() + "() with arguments: "
                                + Arrays.toString(args));
                        if (method.getName().equals("sum")) {
                            logger.info(String.valueOf(Arrays.stream(args).mapToInt(value -> (int) value).sum()));
                        }
                    }
                    if (levelLog.equals("info")) {
                        if (method.getName().equals("mult")) {
                            logger.debug("Was called method: " + method.getName() + "() with arguments: "
                                    + Arrays.toString(args));
                            logger.info(String.valueOf(Arrays.stream(args).mapToInt(a -> (int) a)
                                    .reduce(1, (a, b) -> a * b)));
                        }
                    }
                    return method.invoke(bean, args);
                }
        );
    }
}

// END
