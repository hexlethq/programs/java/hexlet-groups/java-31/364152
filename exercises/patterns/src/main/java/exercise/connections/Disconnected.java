package exercise.connections;

import exercise.TcpConnection;

import java.util.Objects;

// BEGIN
public class Disconnected implements Connection {
    private static TcpConnection tcpConnection;
    private static String status;

    @Override
    public String getCurrentState() {
        return status;
    }

    @Override
    public void connect() {
    tcpConnection.connect();
    }

    @Override
    public void disconnect() {
        if (Objects.equals(status,"disconnected")) {
            System.out.println("Error");
        }
        status = "disconnected";
    }

    @Override
    public void write(String str) {
        System.out.println("Error");
    }
}
// END
