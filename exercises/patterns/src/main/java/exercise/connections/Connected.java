package exercise.connections;

import exercise.TcpConnection;

import java.util.Objects;

// BEGIN
public class Connected implements Connection {

   private TcpConnection tcpConnection;
   private static String status;


    @Override
    public String getCurrentState() {
        return status;
    }

    @Override
    public void connect() {
        if (Objects.equals(status,"connected")) {
            System.out.println("Error");
        }
        status = "connected";
    }

    @Override
    public void disconnect() {
        tcpConnection.disconnect();
    }

    @Override
    public void write(String str) {

    }
}
// END
