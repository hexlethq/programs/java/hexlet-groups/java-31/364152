package exercise;

import exercise.connections.Connected;
import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {
    private final String ip;
    private final int port;
    private static Connection connection;

    public TcpConnection(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getCurrentState() {
        return connection.getCurrentState();
    }

    public void connect() {
        connection = new Connected();
        connection.connect();
    }

    public void disconnect() {
        connection = new Disconnected();
        connection.disconnect();
    }

    public void write(String str) {
        if(connection.getCurrentState().equals("disconnected")) {
            System.out.println("Error");
        }
        connection.write(str);
    }
}
// END
