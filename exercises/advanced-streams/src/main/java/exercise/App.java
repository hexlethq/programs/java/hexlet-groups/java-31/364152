package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String getForwardedVariables(String config) {

        return config.lines()
                .filter(configFilter -> configFilter.startsWith("environment"))
                .filter(configFilter -> configFilter.contains("X_FORWARDED_"))
                .flatMap(str -> Stream.of(str.split(",")))
                .map(str -> str.replaceAll("environment=", " ").trim())
                .map(str -> str.replaceAll("\"", " ").trim())
                .filter(strFilter -> strFilter.startsWith("X_FORWARDED_"))
                .map(str -> str.replaceAll("X_FORWARDED_", " ").trim())
                .collect(Collectors.joining(","));
    }
}
//END
