package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] str) {
        StringBuilder html = new StringBuilder();
        if (str.length == 0) {
            return html.toString();
        }
        StringBuilder htmlResult = new StringBuilder("<ul>\n");
        for (int i = 0; i < str.length; i++) {
            htmlResult = (htmlResult.append("  <li>").append(str[i]).append("</li>\n"));
        }
        htmlResult = htmlResult.append("</ul>");
        return htmlResult.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        int countOfMatches = 0;
        StringBuilder htmlResult = new StringBuilder("<ul>\n");
        for (int i = 0; i < users.length; i++) {
            LocalDate date = LocalDate.parse(users[i][1]);
            int year1 = date.getYear();
            if (year1 == year) {
                htmlResult = (htmlResult.append("  <li>").append(users[i][0]).append("</li>\n"));
            } else if (year1 != year) {
                countOfMatches++;
            }
        }
        if (countOfMatches == users.length) {
            return "";
        }
        htmlResult = htmlResult.append("</ul>");
        return htmlResult.toString();
    }
    // END
}
