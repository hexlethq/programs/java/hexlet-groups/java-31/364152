package exercise;

import exercise.domain.User;
import exercise.domain.query.QUser;
import kong.unirest.Body;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import io.javalin.Javalin;
import io.ebean.DB;
import io.ebean.Transaction;

import java.util.Optional;

class AppTest {

    private static Javalin app;
    private static String baseUrl;
    private static Transaction transaction;

    // BEGIN
    @BeforeAll
    public static void beforeAll() {
        app = App.getApp();
        app.start(0);
        int port = app.port();
        baseUrl = "http://localhost:" + port;
    }

    @AfterAll
    public static void afterAll() {
        app.stop();
    }
    // END

    // Хорошей практикой является запуск тестов с базой данных внутри транзакции.
    // Перед каждым тестом транзакция открывается,
    @BeforeEach
    void beforeEach() {
        transaction = DB.beginTransaction();
    }

    // А после окончания каждого теста транзакция откатывается
    // Таким образом после каждого теста база данных возвращается в исходное состояние,
    // каким оно было перед началом транзакции.
    // Благодаря этому тесты не влияют друг на друга
    @AfterEach
    void afterEach() {
        transaction.rollback();
    }

    @Test
    void testRoot() {
        HttpResponse<String> response = Unirest.get(baseUrl).asString();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void testUsers() {

        // Выполняем GET запрос на адрес http://localhost:port/users
        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users")
            .asString();
        // Получаем тело ответа
        String content = response.getBody();

        // Проверяем код ответа
        assertThat(response.getStatus()).isEqualTo(200);
        // Проверяем, что на станице есть определенный текст
        assertThat(content).contains("Wendell Legros");
        assertThat(content).contains("Larry Powlowski");
    }

    @Test
    void testUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/5")
            .asString();
        String content = response.getBody();

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(content).contains("Rolando Larson");
        assertThat(content).contains("galen.hickle@yahoo.com");
    }

    @Test
    void testNewUser() {

        HttpResponse<String> response = Unirest
            .get(baseUrl + "/users/new")
            .asString();

        assertThat(response.getStatus()).isEqualTo(200);
    }

    // BEGIN
    @Test
    void testCreateUser() {
        String actualFirstName = "Petr";
        String actualLastName = "Ivanov";
        String actualEmail = "example@mail.com";
        String actualPassword = "password";

        HttpResponse responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName",actualFirstName)
                .field("lastName",actualLastName)
                .field("email",actualEmail)
                .field("password",actualPassword)
                .asEmpty();
        assertThat(responsePost.getStatus()).isEqualTo(302);
        assertThat(responsePost.getHeaders().getFirst("Location")).isEqualTo("/users");

        User actualUser = new QUser()
                .firstName.equalTo(actualFirstName)
                .findOne();

        assertThat(actualUser).isNotNull();
        assertThat(actualUser.getFirstName()).isEqualTo(actualFirstName);
        assertThat(actualUser.getLastName()).isEqualTo(actualLastName);
        assertThat(actualUser.getEmail()).isEqualTo(actualEmail);
        assertThat(actualUser.getPassword()).isEqualTo(actualPassword);

    }
    // END
    @Test
    void testCreateUserNegative() {
        String actualFirstName = "";
        String actualLastName = "";
        String actualEmail = "examplemail.com";
        String actualPassword = "ps";

        HttpResponse<String> responsePost = Unirest
                .post(baseUrl + "/users")
                .field("firstName", actualFirstName)
                .field("lastName", actualLastName)
                .field("email", actualEmail)
                .field("password", actualPassword)
                .asString();

        assertThat(responsePost.getStatus()).isEqualTo(422);

        User actualUser = new QUser()
                .email.equalTo(actualEmail)
                .findOne();
        assertThat(actualUser).isNull();

        String content = responsePost.getBody();

        assertThat(content).contains("Имя не должно быть пустым");
        assertThat(content).contains("Фамилия не должна быть пустой");
        assertThat(content).contains("Должно быть валидным email");
        assertThat(content).contains("Пароль должен содержать не менее 4 символов");
        assertThat(content).contains(actualEmail);
        assertThat(content).contains(actualPassword);
    }
}
