package exercise.controller;

import exercise.model.Course;
import exercise.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Arrays;


@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseRepository courseRepository;

    @GetMapping(path = "")
    public Iterable<Course> getCorses() {
        return courseRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Course getCourse(@PathVariable long id) {
        return courseRepository.findById(id);
    }

    // BEGIN
    @GetMapping(path = "/{id}/previous")
    public List<Course> getTree(@PathVariable long id) {
        Course course = courseRepository.findById(id);
        if (course.getPath() == null) {
            return List.of();
        }

        List<Course> treeList = new java.util.ArrayList<>(Arrays.stream(course.getPath().split("\\."))
                .toList()
                .stream()
                .map(x -> courseRepository.findById(Long.parseLong(x))).toList());

       treeList.add(course);

        return treeList;
    }
    // END

}
