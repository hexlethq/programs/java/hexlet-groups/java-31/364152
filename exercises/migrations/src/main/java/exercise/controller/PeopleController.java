package exercise.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/people")
public class PeopleController {
    @Autowired
    JdbcTemplate jdbc;

    @PostMapping(path = "")
    public void createPerson(@RequestBody Map<String, Object> person) {
        String query = "INSERT INTO person (first_name, last_name) VALUES (?, ?)";
        jdbc.update(query, person.get("first_name"), person.get("last_name"));
    }

    // BEGIN
    @GetMapping(path = "")
    public JSONPObject showPeople() throws SQLException {
        String query = "SELECT * FROM person";

        RowMapper<Map<String,String>> rowMapper = new RowMapper<Map<String,String>>() {
            @Override
            public Map<String,String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                return Map.of("id", rs.getString("id"),
                        "first_name", rs.getString("first_name"),
                        "last_name", rs.getString("last_name"));
            }
        };

        List<Map<String,String>> people = jdbc.query(query,rowMapper);
        return new JSONPObject("",people);
    }

    @GetMapping("/{id}")
    public JSONPObject showPerson(@PathVariable String id) throws SQLException {
        String query = "SELECT * FROM person WHERE id =" + id;

        RowMapper<Map<String,String>> rowMapper = new RowMapper<Map<String,String>>() {
            @Override
            public Map<String,String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                return Map.of("id", rs.getString("id"),
                        "first_name", rs.getString("first_name"),
                        "last_name", rs.getString("last_name"));
            }
        };

        List<Map<String,String>> person = jdbc.query(query,rowMapper);
        return new JSONPObject("",person);

        // END
    }
}
