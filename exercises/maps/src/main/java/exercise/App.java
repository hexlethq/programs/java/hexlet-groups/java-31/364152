package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

// BEGIN
class App {
    public static Map getWordCount(String sentence) {
        String[] splitSentence = sentence.split(" ");
        HashMap<String, Integer> result = new HashMap<>();
        if (sentence.length() == 0) {
            return result;
        }
        int coincidence = 0;
        for (int i = 0; i < splitSentence.length; i++) {
            coincidence = 0;
            for (int j = 0; j < splitSentence.length; j++) {
                if (splitSentence[i].equals(splitSentence[j])) {
                    coincidence++;
                }
            }
            result.put(splitSentence[i], coincidence);
        }
        return result;
    }

    public static String toString(Map sentence) {
        if (sentence.isEmpty())
            return "{}";

        Set<Map.Entry<String, Integer>> setKeyAndValue = sentence.entrySet();
        String result = "{" + "\n";
        for (Map.Entry<String, Integer> getKeyAndValue : setKeyAndValue) {

            result = result + "  " + getKeyAndValue.getKey() + ": " + getKeyAndValue.getValue() + "\n";
        }
        return result + "}";
    }

}
//END
