package exercise;

import java.util.List;

// BEGIN
class App {
    public static Integer getCountOfFreeEmails(List<String> emailsList) {
        if (emailsList != null) {
            Long yandexFilter = emailsList.stream()
                    .filter(email -> email.contains("@yandex.ru"))
                    .count();
            Long gmailFilter = emailsList.stream()
                    .filter(email -> email.contains("@gmail.com"))
                    .count();
            Long hotmailFilter = emailsList.stream()
                    .filter(email -> email.contains("@hotmail.com"))
                    .count();
            return (int) (yandexFilter + gmailFilter + hotmailFilter);
        }
        return 0;
    }
}
// END
