package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

// BEGIN
class App {
    public static Map<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        Set<String> data1AndData2 = new TreeSet<>();
        data1AndData2.addAll(data1.keySet());
        data1AndData2.addAll(data2.keySet());
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        for (String s :
                data1AndData2) {
            if (!data1.containsKey(s) && data2.containsKey(s)) {
                result.put(s, "added");
            } else if (data1.containsKey(s) && !data2.containsKey(s)) {
                result.put(s, "deleted");
            } else if (data1.containsKey(s) && data2.containsKey(s)) {
                if (data1.get(s).equals(data2.get(s))) {
                    result.put(s, "unchanged");
                } else result.put(s, "changed");
            }
        }
        return result;
    }
}
//END
