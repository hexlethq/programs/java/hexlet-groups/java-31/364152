package exercise;

// BEGIN
public class Segment {
    private final Point firstPoint;
    private final Point secondPoint;

    public Segment(Point firstPoint, Point secondPoint) {
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
    }

    public Point getBeginPoint() {
        return firstPoint;
    }

    public Point getEndPoint() {
        return secondPoint;
    }

    public Point getMidPoint() {
        return new Point((firstPoint.getX() + secondPoint.getX()) / 2,
                (firstPoint.getY() + secondPoint.getY()) / 2);
    }
}
// END
