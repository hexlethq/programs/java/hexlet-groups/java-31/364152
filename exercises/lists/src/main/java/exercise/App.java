package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// BEGIN
class App {
    public static boolean scrabble(String letters, String word) {
        int sizeLetters = letters.length() - 1;
        if (sizeLetters == 0) {
            return false;
        }
        boolean result = true;
        String[] wordSplit = word.trim().toUpperCase().split("");
        String[] lettersSplit = letters.trim().toUpperCase().split("");
        List<String> listOfWordSplit = new ArrayList<>(Arrays.asList(wordSplit));
        List<String> listOfLettersSplit = new ArrayList<>(Arrays.asList(lettersSplit));
        for (String symbol : listOfWordSplit) {
            if (result) {
                result = listOfLettersSplit.remove(symbol);
            }
        }
        return result;
    }
}
//END
