package exercise.controller;

import exercise.model.Article;
import exercise.repository.ArticleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/articles")
public class ArticlesController {

    @Autowired
    private ArticleRepository articleRepository;

    @GetMapping(path = "")
    public Iterable<Article> getArticles() {
        return this.articleRepository.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public void deleteArticle(@PathVariable long id) {
        this.articleRepository.deleteById(id);
    }

    // BEGIN
    @PostMapping(path = "")
    public void createArticle(@RequestBody Article article) {
        articleRepository.save(article);
    }

    @PatchMapping(path = "/{id}")
    public void updateArticle(@RequestBody Article article, @PathVariable long id) {
        Article articleUpdate = articleRepository.findById(id);
        articleUpdate.setName(article.getName());
        articleUpdate.setBody(article.getBody());
        articleUpdate.setCategory(article.getCategory());
        articleRepository.save(articleUpdate);
    }

    @GetMapping(path = "/{id}")
    public Article showArticle(@PathVariable long id) {
        return articleRepository.findById(id);
    }
    // END
}
