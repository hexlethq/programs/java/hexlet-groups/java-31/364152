package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        ObjectMapper mapper = new ObjectMapper(new JsonFactory());
        final Path fileForParse = Paths.get("src/main/resources/users.json").normalize()
                .toAbsolutePath();
        final String content = Files.readString(fileForParse);
        return mapper.readValue(content, new TypeReference<>() {
        });
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        List<LinkedHashMap<String, String>> data = getUsers();
        final List<String> listUsers = new ArrayList<>();
        for (LinkedHashMap<String, String> word : data) {
            listUsers.add("<tr><td>" + word.get("id") + "</td><td><a" +
                    " href=\"/users/" + word.get("id") + "\">"
                    + word.get("firstName") + " " + word.get("lastName") + "</a></td></tr>");
        }
        StringBuilder fullName = new StringBuilder();
        for (String user : listUsers) {
            fullName.append(user);
        }

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();


        out.println("<html><body><table><tr><th> id </th><th> fullName </th></tr>" +
                fullName + "</table></body></html>");

    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        List<LinkedHashMap<String, String>> date = getUsers();

        String result = "";
        for (LinkedHashMap<String, String> word : date) {
            if (word.get("id").equals(id)) {
                result = "<tr><td>" + word.get("firstName") +
                        "</td><td>" + word.get("lastName") +
                        "</td ><td >" + word.get("email") + "</td ></tr>";
            }
        }

        if (result.length() == 0) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        out.write("<html><body><table><tr><th>firstName</th><th>lastName</th><th>email</th></tr>" +
                result + "</table></body></html>");

        // END
    }
}
