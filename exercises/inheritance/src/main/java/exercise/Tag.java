package exercise;

import java.util.Set;
import java.util.Map;

// BEGIN
public class Tag {
    public String tagName;
    public Map<String, String> tagAttribute;

    public String toString() {
        Set<String> setKey = tagAttribute.keySet();
        String result = "<" + tagName;
        for (String key : setKey) {
            result = result + String.format(" %s=\"%s\"", key, tagAttribute.get(key));
        }
        result += ">";
        return result.trim();
    }

}
// END
