package exercise;

import java.util.Map;
import java.util.List;

// BEGIN
public class PairedTag extends Tag {

    private final String tagBody;
    private final List<Tag> children;

    public PairedTag(String tagName, Map<String, String> tagAttribute, String tagBody, List<Tag> children) {
        super.tagName = tagName;
        super.tagAttribute = tagAttribute;
        this.tagBody = tagBody;
        this.children = children;
    }


    @Override
    public String toString() {
        String result = super.toString() + tagBody;
        for (Tag tag : children) {
            result += tag.toString();
        }
        result += "</" + super.tagName + ">";
        return result.trim();
    }
}
// END
