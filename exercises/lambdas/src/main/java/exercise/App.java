package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] image) {
        String[][] arr = Arrays.stream(image)
                .flatMap(arr1 -> Stream.of(arr1, arr1))
                .map(str -> Arrays.stream(str).flatMap(symbol -> Stream.of(symbol, symbol)))
                .map(arr1 -> arr1.toArray(String[]::new))
                .toArray(String[][]::new);

        return arr;
    }

}
// END
