package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = new int[2];
        point[0] = x;
        point[1] = y;
        return point;
    }

    public static int getX(int[] point) {
        int x = point[0];
        return x;
    }

    public static int getY(int[] point) {
        int y = point[1];
        return y;
    }

    public static String pointToString(int[] point) {
        String str = "(" + point[0] + "," + " " + point[1] + ")";
        return str;
    }

    public static int getQuadrant(int[] point) {
        int x = point[0];
        int y = point[1];
        if (x > 0 && y > 0) {
            return 1;
        } else if (x > 0 && y < 0) {
            return 4;
        } else if (x < 0 && y < 0) {
            return 3;
        } else if (x < 0 && y > 0) {
            return 2;
        }
        return 0;
    }
    // END
}
