package exercise.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.Map;
import java.util.List;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping("/cities/{id}")
    public Map<String, Object> getCity(@PathVariable long id) throws JsonProcessingException {

        City city = cityRepository.findById(id).get();
        Map<String, Object> cityInfo = new HashMap<>();
        cityInfo.put("name", city.getName());
        cityInfo.put("temperature", weatherService.weatherCity(city.getName()).get("temperature"));
        cityInfo.put("cloudy", weatherService.weatherCity(city.getName()).get("cloudy"));
        cityInfo.put("humidity", weatherService.weatherCity(city.getName()).get("humidity"));
        cityInfo.put("wind", weatherService.weatherCity(city.getName()).get("wind"));

        return cityInfo;

    }

    @GetMapping("/search")

    public List<Map<String, Object>> getCities(@RequestParam(defaultValue = "") String name) {


        if (name.length() != 0) {

            return cityRepository.findByNameStartingWithIgnoreCase(name).stream()
                    .map(x -> {
                        try {
                            return Map.of("name", x.getName(),
                                    "temperature", weatherService.weatherCity(x.getName()).get("temperature"));
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }).toList();

        }

        return cityRepository.OrderByName().stream()
                .map(x -> {
                    try {
                        return Map.of("name", x.getName(),
                                "temperature", weatherService.weatherCity(x.getName()).get("temperature"));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).toList();
    }


    // END
}

