package exercise;
import java.util.Arrays;
class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] number) {
        if (number.length == 0) {
            return -1;
        }
        int value = 0;
        int index = 0;
        int min = Integer.MIN_VALUE;
        for (int i = 0; i < number.length; i++) {
            if (number[i] < 0 && number[i] > min) {
                min = number[i];
                index = i;
            } else if (number[i] >= 0) {
                value++;
            }
            if (value == number.length) {
                return -1;
            }

        }
        for (int i = 0; i < number.length; i++) {
            if (number[i] == min) {
                index = i;
                break;
            }
        }
        return index;
    }

    public static int[] getElementsLessAverage(int[] number) {
        if (number.length == 0) {
            return number;
        }
        int[] number1 = new int[number.length];
        double amount = 0;
        for (int i = 0; i < number.length; i++) {
            amount = amount + number[i];
        }
        double arithmeticMean = amount / number.length;
        int arrLength = 0;
        for (int i = 0, j = 0; i < number.length; i++) {
            if (number[i] < arithmeticMean) {
                number1[j] = number[i];
                j++;
                arrLength = j;
            }
        }
        int[] number2 = Arrays.copyOf(number1, arrLength);
        return number2;
    }
    // END
}
